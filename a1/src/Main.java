import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        HashMap<String,Integer> games = new HashMap<>();
        games.put("Mario Odyssey",50);
        games.put("Super Smash Bros. Ultimate",20);
        games.put("Luigi's Mansion 3",15);
        games.put("Pokemon Sword",30);
        games.put("Pokemon Shield",100);

        games.forEach((key, value) -> {
            System.out.println(key +" has " +value +" stocks left.");
        });

        ArrayList<String> topGames = new ArrayList<>();
        games.forEach((key,value) -> {
            if(value <= 30 ){
                topGames.add(key);
                System.out.println(key +" has been added to the top games list!");
            }
        });
        System.out.println("Our shop's top games:");
        System.out.println(topGames);

        boolean addItem = true;
        Scanner scanner = new Scanner(System.in);
        while(addItem == true){
            System.out.println("Would you like to add an item? Yes or No.");
            String input = scanner.nextLine();
            if(input.equals("Yes")){
                System.out.println("Add the item name:");
                String itemName = scanner.nextLine();

                System.out.println("Add the item stock:");
                int itemStock = scanner.nextInt();

                games.put(itemName,itemStock);
                addItem = false;
                System.out.println(games);
            } else if (input.equals("No")) {
                System.out.println("Thank you.");
            }
        }
    }
}