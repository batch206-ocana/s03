import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        //while loop
            int a = 1;
            while(a < 5 ){
                System.out.println("While loop counter: " +a);
                a++;
            }
        //do-while loop
            int b = 5;
            do{
                System.out.println("Countdown: " +b);
                b--;
            } while(b > 0);
        //for loop
            int[] newArr = {100,200,300,400,500};
            for(int i = 0; i < newArr.length ; i ++){
                System.out.println("Item at index number " +i +" is " +newArr[i]);
            }

            String[][] classroom = new String[3][3];
            classroom[0][0] = "Rayquaza";
            classroom[0][1] = "Kyogre";
            classroom[0][2] = "Groudon";
            classroom[1][0] = "Sora";
            classroom[1][1] = "Goofy";
            classroom[1][2] = "Donald";
            classroom[2][0] = "Harry";
            classroom[2][1] = "Ron";
            classroom[2][2] = "Hermione";
            System.out.println(Arrays.deepToString(classroom)); // used to display the values of a multidimensional array
            for(int row = 0; row < 3; row++){
                for (int column = 0; column < 3; column++){
                    System.out.println(classroom[row][column]);
                }
            }
            //Enhanced for loop (forEach)
            String[] members = {"Eugene","Vincent","Dennis","Alfred"};
            for(String member:members){
                System.out.println(member);
            }
            for(String[] row: classroom){
                for (String student: row){
                    System.out.println(student);
                }
            }

            HashMap<String,String> techniques = new HashMap<>();
            techniques.put(members[0],"Spirit Gun");
            techniques.put(members[1],"Black Dragon");
            techniques.put(members[2],"Rose Whip");
            techniques.put(members[3],"Spirit Sword");
            System.out.println(techniques);

            techniques.forEach((key,value) -> {
                System.out.println("Member " +key +" uses " +value);
            });
        //Exception handling
            System.out.println("Enter an integer: ");
            try{
                int num = scanner.nextInt();
            } catch(Exception e){
                System.out.println("Invalid Input");

            }
            System.out.println("Hello from the outside");
    }
}